<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
</head>
<body>
<form:form name='r' action='/register' method='POST' modelAtttribute="user">
    <tr>
        <td>User:</td>
        <td><form:input type='text' name='username' path="username" value=''/></td>
        <br>
    </tr>
    <tr>
        <td>Password:</td>
        <td><form:input type='password' name='password' path="password"/></td>
        <br>
    </tr>
    <tr>
        <td>Email:</td>
        <td><form:input type='email' name='email' path="email"/></td>
        <br>
    </tr>
    <tr>
        <td><input name="submit" type="submit" value="Zarejestruj sie"/></td>
    </tr>
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form:form>
</body>
</html>