<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="${_csrf.token}">
    <link href="/static/game.css" rel="stylesheet"/>
</head>
<body onLoad="initGame()">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/static/request_utils.js"></script>
<script src="/static/in_game.js"></script>

<div id="top-container">
    <security:authentication property="principal.username" var="username"/>
</div>
<div id="left-container">

</div>
<div id="right-container">
    <ul id="players">

    </ul>
    <%--<button id="play" type="button" onclick="searchOneOnOne()"> Szukaj 1na1 </button>--%>
</div>
<div id="mid-container">

    <b><p id="questionText"></p></b>

    <div id="game-container">
        <div id="quote-container">
            <p id="quote">

            </p>
        </div>
        <div id="answers-container">
            <form>
                <input type="checkbox" id="A" class="answers-checkbox">
                <label for="A" id="label-A"> </label>
                <input type="checkbox" id="B" class="answers-checkbox">
                <label for="B" id="label-B"> </label>
                <input type="checkbox" id="C" class="answers-checkbox">
                <label for="C" id="label-C"> </label>
                <br>
                <input type="checkbox" id="D" class="answers-checkbox">
                <label for="D" id="label-D"> </label>
                <input type="checkbox" id="E" class="answers-checkbox">
                <label for="E" id="label-E"> </label>
                <input type="checkbox" id="F" class="answers-checkbox">
                <label for="F" id="label-F"> </label>
            </form>
        </div>
        <div id="button-container">
            <button id="select-answer" type="button" onClick="submitAnswer()"> Zatwierdź</button>
        </div>
        <div id="audio-container">
            <audio id="quote_player" autoplay controls>
                <source src="" type="audio/wav">
            </audio>
        </div>
    </div>

</div>

</body>
</html>