<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="${_csrf.token}">
    <link href="/static/home.css" rel="stylesheet"/>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/static/queue.js"></script>
<script src="/static/game.js"></script>
<script src="/static/request_utils.js"></script>

<div id="top-container">
    <security:authentication property="principal.username" var="username"/>.
    <h1 id="welcomeMsg"> Tak wiec, boskie dziecie, udalo ci sie zalogowac. Witaj, ${username} </h1>
</div>
<div id="left-container">

</div>
<div id="right-container">
    <button id="play" type="button" onclick="searchOneOnOne()"> Szukaj 1na1</button>
</div>
<div id="mid-container">

    <div id="game-desc-container">
        <article>No tom wykopał mu uszy ze łba aż zdech, hehe.</article>
    </div>

</div>

</body>
</html>