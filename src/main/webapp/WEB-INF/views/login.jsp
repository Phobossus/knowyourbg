<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link href="/static/login.css" rel="stylesheet"/>
</head>
<body>
<h1>Login</h1>
<div id="loginBox" align="center">
    <form id='login' name='f' action='/login' method='POST'>
        <table>
            <tr>
                <td>Nazwa uzytkownika:</td>
            </tr>
            <tr>
                <td><input type='text' name='username' value=''></td>
            </tr>
            <tr>
                <td>Haslo:</td>
            </tr>
            <tr>
                <td><input type='password' name='password'/></td>
            </tr>
            <tr>
                <td><input name="submit" type="submit" value="Zaloguj sie"/></td>
            </tr>
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
        </table>
    </form>
</div>

<p> Nie masz konta? Tu mozesz to zmienic: </p>
<form action="/register" method='GET'>
    <input name="register-button" type="submit" value="Zarejestruj sie"/>
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>

</body>
</html>