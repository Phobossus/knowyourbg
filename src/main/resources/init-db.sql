-- To properly initialize a database, execute this script in mysql shell:

CREATE database bgdb;
USE bgdb;
CREATE TABLE User (
id bigint NOT NULL AUTO INCREMENT PRIMARY KEY,
username varchar(30) UNIQUE,
password varchar(60),
email varchar(60) UNIQUE,
statid bigint
);
CREATE TABLE Statistics (
id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
wins integer(5),
loses integer(5)
);
ALTER TABLE User
ADD FOREIGN KEY (statid)
REFERENCES Statistics(id)
ON DELETE CASCADE;