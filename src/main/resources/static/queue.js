function searchOneOnOne() {
    console.log("Queuing");
    var request = {"gameMode": "solo"};

    $.ajax({
        url: "/search/solo",
        type: "POST",
        data: JSON.stringify(request),
        headers: getDefaultHeaders(),
        crossDomain: true,
        contentType: "application/json",
        datatype: "json"
    })
        .done(function (response) {
            if(response.searchState === "PENDING") {
                alert("Rozpocząłeś szukanie gry 1na1!");
                setTimeout(function () {
                    getSearchState();
                }, 3000);
            } else if(response.searchState === "CANCELLED") {
                alert("Przerwałeś szukanie gry 1na1!");
            }
        })
        .fail(function (xhr, status, error) {
            console.log("Error occurred while trying to start solo match search.");
            alert(xhr.responseText);
        })
}

function getSearchState() {
    console.log("Searching...");
    var completed = false;

    $.ajax({
        url: "/search/solo/state",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    })
        .done(function (response) {
            completed = response.searchState === "FOUND";
            if (completed) {
                var message = "Znaleziono przeciwnika! Grasz?";
                // 20 sekund na zaakceptowanie
                if (confirm(message)) {
                    joinGame()
                } else {
                    // send notification that the game should be cancelled
                }
            }
        })
        .fail(function (xhr, status, error) {
            console.log("Error occurred while checking for started match: " + search_id);
            alert(xhr.responseText);
        })
        .always(function (response) {
            if (!completed && response.searchState !== "NONEXISTING") {
                setTimeout(function () {
                    getSearchState();
                }, 3000);
            }
        });
}

// once there is a match and all players receive a "FINISHED" response, they should send a request to
// 1) create a Game
// 2) get the id of the game
// 3)
