

function getDefaultHeaders() {
    return {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')}
}

function getGameId() {
    return window.location.pathname.split("/game/")[1];
}

function redirect(relativeUrl) {
    window.location.href = relativeUrl;
}