function joinGame() {
    console.log("Joining game...");

    $.ajax({
        url: "/game/join",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    })
        .done(function(response){
            alert("Succesfully joined new game");
            renderJoinedGame(response);
        })
        .fail(function (xhr, status, error) {
            console.log("Error occurred while tryin to join the game");
            alert(xhr.responseText);
        })
}

function renderJoinedGame(response) {
    window.location.href = response.gameUrl;
    console.log("Rendering url: " + window.location.href);
}