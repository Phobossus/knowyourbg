expectedPlayers = {};

function initGame() {
    waitUntilStart(getGameId());
}

function refreshGame(gameId) {
    console.log("Refreshing... ");
    $.ajax({
        url: "/game/" + gameId + "/refresh",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    })
        .done(function (response) {
            updateJoinedGame(response);
        })
        .fail(function (xhr, status, error) {
            console.log("Error occurred while refreshing the game");
            alert(xhr.responseText);
        })
        .always(function () {
            setTimeout(function () {
                refreshGame(gameId);
            }, 2000);
        })
}

function waitUntilStart(gameId) {
    console.log("Checking game status... ");

    $.ajax({
        url: "/game/" + gameId + "/refresh",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    })
        .done(function (response) {
            var gameId = response.gameId;
            updateJoinedGame(response);
            if (!response.started) {
                setTimeout(function () {
                    waitUntilStart(gameId);
                }, 2000);
            } else {
                console.log("Game full. Getting the first quote... ");
                nextQuestion(gameId);
            }
        })
        .fail(function (xhr, status, error) {
            console.log("Error occurred while refreshing the game");
            alert(xhr.responseText);
        })
}

function nextQuestion(gameId) {
    $.ajax({
        url: "/game/" + gameId + "/question",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    })
        .done(function (response) {
            var questionText = response.questionText;
            var quote = response.questionObject.text;
            var answers = response.answersText;
            var remaining = response.remainingQuestionsCount;
            renderQuestion(questionText, quote, answers);
            if (remaining === 0) {
                setTimeout(function () {
                    finalizeGame(gameId);
                    redirect("/home");
                }, 7000);
            }
        })
        .fail(function (response) {
            console.log("Failed to retrieve next question. Retrying.");
            setTimeout(function () {
                nextQuestion(gameId);
            }, 2000);
        });

    function renderQuestion(questionText, quote, answers) {
        // refactor this lol
        $("#questionText").text(questionText);

        $("#quote").text(quote);

        $("#label-A").text(answers[0]);
        $("#label-B").text(answers[1]);
        $("#label-C").text(answers[2]);
        $("#label-D").text(answers[3]);
        $("#label-E").text(answers[4]);
        $("#label-F").text(answers[5]);
    }
}

function submitAnswer() {
    var gameId = getGameId();
    console.log("Submitting answer...");
    var answers = [];
    $('.answers-checkbox:checkbox:checked').each(function () {
        answers.push($(this).next('label').text());
    });
    //var request = {"selectedAnswers:": answers};

    $.ajax({
        url: "/game/" + gameId + "/answer",
        type: "POST",
        data: JSON.stringify(answers),
        headers: getDefaultHeaders(),
        crossDomain: true,
        contentType: "application/json",
        datatype: "json"
    })
        .done(function (response) {
            var questionId = response.questionId;
            setTimeout(function () {
                checkAllPlayersSubmitted(gameId, questionId);
            }, 1000);
        })
        .fail(function (response) {
            console.log("Submitting answers has failed. Retrying in 2 sec...");
            // uncomment when it works
            // setTimeout(function () {
            //     submitAnswer();
            // }, 2000)
        });
}

function checkAllPlayersSubmitted(gameId, questionId) {
    $.ajax({
        url: "/game/" + gameId + "/" + questionId + "/completed",
        type: "GET",
        headers: getDefaultHeaders(),
        crossDomain: true,
        datatype: "json"
    }).done(function (response) {
        var questionReady = response.questionReady;
        if (questionReady) {
            nextQuestion(gameId);
        } else {
            setTimeout(function () {
                checkAllPlayersSubmitted(gameId, questionId);
            }, 4000);
        }
    }).fail(function (response) {
        console.log("Check if all players submitted has failed. Retrying...");
        checkAllPlayersSubmitted(gameId, questionId)
    });
}

function finalizeGame(gameId) {
    $.ajax({
        url: "/game/" + gameId,
        type: "DELETE",
        headers: getDefaultHeaders(),
        crossDomain: true,
        //datatype: "json"
    }).done(function (response) {
        if (response === true) {
            console.log("Game succesfuly deleted.");
        } else {
            console.log("Failed to delete the game - it is not over.")
        }
    }).fail(function (response) {
        console.log("Error occurred while trying to delete the game. Retrying... ");
        setTimeout(function () {
            finalizeGame(gameId);
        }, 4000);
    });
}


// function updatePlayerList(expectedPlayers, joinedPlayers) {
//     $("#players").empty();
//     expectedPlayers.forEach(function (player, index) {
//         $("#players").append(player);
//         if (joinedPlayers.indexOf(player) === -1) {
//             $("#players li:eq(index)").text(player + " [WAITING]");
//         } else {
//             $("#players li:eq(index)").text(player + " [READY]");
//         }
//     });
// }

function updateJoinedGame(response) {
    // updatePlayerList(response.expectedPlayers, response.joinedPlayers);
    // this will only be used to show players in game && their current scores for each question
}