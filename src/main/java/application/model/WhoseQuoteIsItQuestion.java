package application.model;

import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@ToString
public class WhoseQuoteIsItQuestion implements Question {

    private final String questionId;
    private final String questionText;
    private final String quote;
    private final Map<String, Boolean> answers;

    public WhoseQuoteIsItQuestion(String questionText,
                                  String quote,
                                  Map<String, Boolean> answers) {
        this.questionId = "Question" + UUID.randomUUID();
        this.questionText = questionText;
        this.quote = quote;
        this.answers = answers;
    }

    @Override
    public String getQuestionId() {
        return questionId;
    }

    @Override
    public String getQuestionText() {
        return questionText;
    }

    @Override
    public QuestionObject getQuestionObject() {
        QuestionObject questionObject = new QuestionObject();
        questionObject.setText(quote);
        return questionObject;
    }

    @Override
    public List<String> getAnswersText() {
        return new ArrayList<>(answers.keySet());
    }

    @Override
    public List<String> getCorrectAnswers() {
        return answers.keySet().stream()
                .filter(answers::get)
                .collect(Collectors.toList());
    }

}
