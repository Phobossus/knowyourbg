package application.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
public class JoinedGameResponse {

    private final String gameId;
    private final List<Player> expectedPlayers;
    private final List<Player> joinedPlayers;
    private final boolean started;
    private final String gameUrl;

    @JsonCreator
    public JoinedGameResponse(@JsonProperty("gameId") String gameId,
                              @JsonProperty("expectedPlayers") List<Player> expectedPlayers,
                              @JsonProperty("joinedPlayers") List<Player> joinedPlayers,
                              @JsonProperty("started") boolean started,
                              @JsonProperty("gameUrl") String gameUrl) {
        this.gameId = gameId;
        this.expectedPlayers = expectedPlayers;
        this.joinedPlayers = joinedPlayers;
        this.started = started;
        this.gameUrl = gameUrl;
    }

    public String getGameId() {
        return gameId;
    }

    public List<Player> getExpectedPlayers() {
        return expectedPlayers;
    }

    public List<Player> getJoinedPlayers() {
        return joinedPlayers;
    }

    public boolean isStarted() {
        return started;
    }

    public String getGameUrl() {
        return gameUrl;
    }
}

