package application.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode
@ToString
public class Score {

    private final Map<Question, ScoreData> questionScores = new HashMap<>();

    public Map<Question, ScoreData> getQuestionScores() {
        return questionScores;
    }
}
