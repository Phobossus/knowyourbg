package application.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class ScoreData {

    private final Question relatedQuestion;
    private final long points;
    private final int correctAnswersSelected;
    private final int correctAnswersTotal;

    public ScoreData(Question relatedQuestion, long points, int correctAnswersSelected, int correctAnswersTotal) {
        this.relatedQuestion = relatedQuestion;
        this.points = points;
        this.correctAnswersSelected = correctAnswersSelected;
        this.correctAnswersTotal = correctAnswersTotal;
    }

    public Question getRelatedQuestion() {
        return relatedQuestion;
    }

    public long getPoints() {
        return points;
    }

    public int getCorrectAnswersSelected() {
        return correctAnswersSelected;
    }

    public int getCorrectAnswersTotal() {
        return correctAnswersTotal;
    }
}
