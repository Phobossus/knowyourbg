package application.model;

import java.util.List;

public interface Question {

    String getQuestionId();

    String getQuestionText();

    QuestionObject getQuestionObject();

    List<String> getAnswersText();

    List<String> getCorrectAnswers();

}
