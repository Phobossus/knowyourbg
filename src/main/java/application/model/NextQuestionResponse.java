package application.model;

import java.util.List;

public interface NextQuestionResponse {

    String getQuestionText(); // ie. Who said that?

    QuestionObject getQuestionObject(); // ie. actual quote

    List<String> getAnswersText(); // ie. Minsc, Pan Cienia, Sarevok

    long getRemainingQuestionsCount();

}
