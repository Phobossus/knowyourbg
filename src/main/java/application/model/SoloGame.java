package application.model;

import com.google.common.collect.ImmutableList;

import java.util.*;

public class SoloGame implements Game {

    private final List<Player> expectedPlayers;
    private final String gameId;
    private List<Player> joinedPlayers;

    public SoloGame(Player firstPlayer, Player secondPlayer) {
        this.expectedPlayers = new ImmutableList.Builder<Player>()
                .add(firstPlayer)
                .add(secondPlayer)
                .build();
        this.gameId = UUID.randomUUID().toString();
        this.joinedPlayers = new LinkedList<>();
    }

    @Override
    public List<Player> getExpectedPlayers() {
        return expectedPlayers;
    }

    @Override
    public String getGameId() {
        return gameId;
    }

    @Override
    public List<Player> getJoinedPlayers() {
        return joinedPlayers;
    }

    @Override
    public boolean isStarted() {
        return joinedPlayers.containsAll(expectedPlayers);
    }
    // note: this is o(n^2), should be changed in the future for The Weakest Link games

}
