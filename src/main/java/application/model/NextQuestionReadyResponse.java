package application.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class NextQuestionReadyResponse {

    private final boolean questionReady;

    @JsonCreator
    public NextQuestionReadyResponse(@JsonProperty("questionReady") boolean questionReady) {
        this.questionReady = questionReady;
    }

    public boolean isQuestionReady() {
        return questionReady;
    }
}
