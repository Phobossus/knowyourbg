package application.model;

import java.util.List;
import java.util.Optional;

public interface GameProgress {

    Optional<Question> getCurrentQuestion();

    long getRemainingQuestionCount();

    void markCurrentQuestionSubmitted(Player player);

    void markCurrentQuestionCompleted();

    boolean isQuestionCompleted(String questionId);

    boolean hasAlreadySubmittedAnswer(Player player);

    boolean hasEveryPlayerSubmittedAnswer(List<Player> joinedPlayersList);

    boolean isOver();

}
