package application.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@ToString
@EqualsAndHashCode
public class QuestionStateDetails {

    private final QuestionStatus questionStatus;
    private final Set<Player> answersSubmittedBySet;

    public QuestionStateDetails(QuestionStatus questionStatus) {
        this.questionStatus = questionStatus;
        this.answersSubmittedBySet = new HashSet<>();
    }

    public QuestionStatus getQuestionStatus() {
        return questionStatus;
    }

    public Set<Player> getAnswersSubmittedBySet() {
        return answersSubmittedBySet;
    }

}
