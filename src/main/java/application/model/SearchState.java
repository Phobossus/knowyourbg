package application.model;

public enum SearchState {
    FOUND,
    PENDING,
    CANCELLED,
    NONEXISTING,
}
