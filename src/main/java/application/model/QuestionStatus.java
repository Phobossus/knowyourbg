package application.model;

public enum QuestionStatus {
    PENDING,
    CURRENT,
    COMPLETED
}
