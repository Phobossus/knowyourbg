package application.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class SubmitAnswerResponse {

    private final boolean success;
    private final String questionId;

    @JsonCreator
    public SubmitAnswerResponse(@JsonProperty("success") boolean success,
                                @JsonProperty("questionId") String questionId) {
        this.success = success;
        this.questionId = questionId;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getQuestionId() {
        return questionId;
    }
}
