package application.model;

import java.util.List;

public class NextWhoseQuoteIsItResponse implements NextQuestionResponse {

    private final String questionText;
    private final QuestionObject questionObject;
    private final List<String> answersText;
    private final long remainingQuestionsCount;

    public NextWhoseQuoteIsItResponse(String questionText, QuestionObject questionObject, List<String> answersText,
                                      long remainingQuestionsCount) {
        this.questionText = questionText;
        this.questionObject = questionObject;
        this.answersText = answersText;
        this.remainingQuestionsCount = remainingQuestionsCount;
    }

    @Override
    public String getQuestionText() {
        return questionText;
    }

    @Override
    public QuestionObject getQuestionObject() {
        return questionObject;
    }

    @Override
    public List<String> getAnswersText() {
        return answersText;
    }

    @Override
    public long getRemainingQuestionsCount() {
        return remainingQuestionsCount;
    }
}
