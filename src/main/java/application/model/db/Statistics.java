package application.model.db;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class Statistics {
    private long id;
    private int wins;
    private int loses;

    public void setId(long id) {
        this.id = id;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public long getId() {
        return id;
    }

    public int getWins() {
        return wins;
    }

    public int getLoses() {
        return loses;
    }
}
