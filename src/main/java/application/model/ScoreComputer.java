package application.model;

import java.util.List;
import java.util.Map;

public interface ScoreComputer {

    void compute(Player player, Question question, List<String> selectedAnswers);

    Map<Player, Score> getScores();

}
