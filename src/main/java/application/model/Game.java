package application.model;

import java.util.List;

public interface Game {

    List<Player> getExpectedPlayers();

    String getGameId();

    List<Player> getJoinedPlayers();

    boolean isStarted();

}
