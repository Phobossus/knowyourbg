package application.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class SearchStateResponse {

    private final SearchState searchState;

    @JsonCreator
    public SearchStateResponse(@JsonProperty("searchState") SearchState searchState) {
        this.searchState = searchState;
    }

    public SearchState getSearchState() {
        return searchState;
    }

}
