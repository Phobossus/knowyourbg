package application.model;

public enum GameMode {
    RANDOM_SOLO,
    ARRANGED_SOLO,
    THE_WEAKEST_LINK
}
