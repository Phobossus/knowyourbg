package application;

import application.model.db.User;

import java.util.Collection;

public interface UsersCache {

    Collection<User> getUsers();

}
