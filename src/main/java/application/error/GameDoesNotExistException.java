package application.error;

public class GameDoesNotExistException extends RuntimeException {

    public GameDoesNotExistException(String e) {
        super(e);
    }
}
