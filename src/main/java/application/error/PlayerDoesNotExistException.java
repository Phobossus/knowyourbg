package application.error;

public class PlayerDoesNotExistException extends RuntimeException {

    public PlayerDoesNotExistException(String e) {
        super(e);
    }


}
