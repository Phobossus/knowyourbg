package application.error;

public class MatchAlreadyFoundException extends RuntimeException {

    public MatchAlreadyFoundException(String e) {
        super(e);
    }

}
