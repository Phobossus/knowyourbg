package application.error;

public class GameNotStartedException extends RuntimeException {

    public GameNotStartedException(String e) {
        super(e);
    }
}
