package application;

import application.mappers.UserMapper;
import application.model.db.User;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class AllUsersCache implements UsersCache {

    private final UserMapper userMapper;
    private final Collection<User> users;

    public AllUsersCache(UserMapper userMapper) {
        this.userMapper = userMapper;
        this.users = userMapper.selectAllUsers();
    }

    @Override
    public Collection<User> getUsers() {
        return users;
    }
}
