package application.mappers;

import application.model.db.Statistics;

public interface StatisticsMapper {

    int saveStatistics(Statistics statistics);

}
