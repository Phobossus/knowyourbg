package application.mappers;

import application.model.db.User;

import java.util.List;

public interface UserMapper {

    User selectUserByName(String name);

    User selectUserByEmail(String email);

    List<User> selectAllUsers();

    int saveUser(User user);

}
