package application;

import application.model.db.User;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class UserValidator {

    public boolean userExists(User user, Collection<User> allUsers) {
        return allUsers.stream().anyMatch(u -> u.getUsername().equals(user.getUsername()));
    }

}
