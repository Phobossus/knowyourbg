package application;

public enum UserServiceResult {
    SUCCESS(""),
    USER_ALREADY_EXISTS("Uzytkownik o takim imieniu juz istnieje. Zmien nick, albo bede musial ci przywalic solidna dawka cnoty!"),
    GENERAL_ERROR("Dałeś uciec swym myślom. Zaklęcie się nie udało.");

    private String message;

    UserServiceResult(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
