package application.game;

import application.model.Game;
import application.model.GameMode;
import application.model.Player;
import application.model.SoloGame;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class GameMatchManager {

    private final LinkedList<Player> soloQueuePlayers = new LinkedList<>();
    private final List<Game> readyGames = new LinkedList<>();
    private final ScheduledExecutorService scheduledExecutorService;

    public GameMatchManager() {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public void startMatchingPlayers() {
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            System.out.println("Looking for players...");
            matchAvailablePlayers(GameMode.RANDOM_SOLO);
        }, 10, 10, TimeUnit.SECONDS);
    }

    public Optional<Game> getGame(String id) {
        return readyGames.stream().filter(game -> game.getGameId().equals(id))
                .findFirst();
    }

    public Optional<Game> getDedicatedGame(Player player) {
        return readyGames.stream()
                .filter(game -> game.getExpectedPlayers().contains(player))
                .findFirst();
    }

    public boolean isInRandomSoloQueue(Player player) {
        return soloQueuePlayers.contains(player);
    }

    public synchronized void removeFromRandomSoloQueue(Player player) {
        soloQueuePlayers.remove(player);
    }

    public synchronized void addToRandomSoloQueue(Player player) {
        soloQueuePlayers.add(player);
    }

    private synchronized void matchAvailablePlayers(GameMode gameMode) {
        switch (gameMode) {
            case RANDOM_SOLO:
                if (soloQueuePlayers.size() >= 2) {
                    Collections.shuffle(soloQueuePlayers);
                    Player firstPlayer = soloQueuePlayers.removeFirst();
                    Player secondPlayer = soloQueuePlayers.removeLast();
                    readyGames.add(new SoloGame(firstPlayer, secondPlayer));
                }
                break;
            default:
                throw new UnsupportedOperationException("Only solo games are implemented right now.");
        }
    }

    @PreDestroy
    public void onShutDown() {
        scheduledExecutorService.shutdown();
    }

}
