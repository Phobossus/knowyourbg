package application.game;

import application.model.*;
import application.service.CharacterQuoteGenerator;
import application.service.QuestionGenerator;
import application.service.SoloGameProgress;
import application.service.SoloScoreComputer;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PlayGameManager {

    private Map<Game, GameProgress> gameProgressMap = new HashMap<>();
    private Map<Game, ScoreComputer> scoreComputerMap = new HashMap<>();

    public synchronized void initializeGame(Game game) {
        if (!gameProgressMap.containsKey(game)) {
            // future releases: Game will have some properties influencing what type of generator is created
            QuestionGenerator questionGenerator = new CharacterQuoteGenerator();
            gameProgressMap.put(game, new SoloGameProgress(questionGenerator.generate(9)));
            scoreComputerMap.put(game, new SoloScoreComputer(game.getExpectedPlayers()));
        }
    }

    public synchronized boolean finalizeGame(Game game) {
        GameProgress gameProgress = gameProgressMap.get(game);
        boolean over = gameProgress.isOver();
        if (over) {
            if (gameProgressMap.containsKey(game)) {
                gameProgressMap.remove(game);
            }
            if (scoreComputerMap.containsKey(game)) {
                scoreComputerMap.remove(game);
            }
        }
        return over;
    }

    public Optional<Question> getCurrentQuestion(Game game) {
        GameProgress gameProgress = gameProgressMap.get(game);
        return gameProgress.getCurrentQuestion();
    }

    public String submitAnswer(Game game, Player player, List<String> selectedAnswers) {
        GameProgress gameProgress = gameProgressMap.get(game);
        Optional<Question> currentQuestion = gameProgress.getCurrentQuestion();
        if (currentQuestion.isPresent() && !gameProgress.hasAlreadySubmittedAnswer(player)) {
            String questionId = currentQuestion.get().getQuestionId();
            ScoreComputer scoreComputer = scoreComputerMap.get(game);
            scoreComputer.compute(player, currentQuestion.get(), selectedAnswers);
            gameProgress.markCurrentQuestionSubmitted(player);
            // TODO: add OR condition - 45 seconds timer expired,
            // timer starts after rendering question and sending start request
            if (gameProgress.hasEveryPlayerSubmittedAnswer(game.getJoinedPlayers())) {
                gameProgress.markCurrentQuestionCompleted();
            }
            return questionId;
        }
        throw new IllegalStateException("Error submitting answer: no 'current' question exists.");
    }

    public boolean canGetNextQuestion(Game game, String questionId) {
        GameProgress gameProgress = gameProgressMap.get(game);
        return gameProgress.isQuestionCompleted(questionId);
    }

    public long getRemainingQuestionCount(Game game) {
        GameProgress gameProgress = gameProgressMap.get(game);
        return gameProgress.getRemainingQuestionCount();
    }

    public Map<String, Long> getScoresTotal(Game game) {
        if (getRemainingQuestionCount(game) == 0) {
            Map<Player, Score> scores = scoreComputerMap.get(game).getScores();
            Map<String, Long> scoresTotal = new HashMap<>();
            scores.keySet().forEach(player -> {
                Score score = scores.get(player);
                long total = score.getQuestionScores().values().stream().mapToLong(ScoreData::getPoints).sum();
                scoresTotal.put(player.getPlayerName(), total);
            });
            return scoresTotal;
        }
        throw new IllegalStateException("Game must be finished before final results are revealed");
    }

}
