package application.service;

import application.model.Question;

import java.util.List;

public interface QuestionGenerator {

    List<Question> generate(int count);


//    {
//        // should we keep those questions in database? or create them in runtime?
//        // probably runtime so that they arent stealable
//
//        // 1. Query whatever we keep the quotes in
//        // 2. Find who uses the String
//        // 3. Assign him as the correct answer
//        // 4. Generate other names based on (dunno)
//
//        // The algorithm mentioned above would actually be nice to save
//        // the character->List<Quote> mappings so that it is possible
//        // to do generateQuoteFor(Character character) in the future
//        //
//        // Either do this learning lazily or once and for good.
//        Map<String, Boolean> sampleAnswers = new HashMap<>();
//        sampleAnswers.put("Imoen", true);
//        sampleAnswers.put("Sarevok", false);
//        sampleAnswers.put("Jaheira", false);
//        return new WhoseQuoteIsItQuestion("Kto to powiedzial?",
//                new Quote("Czego chcesz?", null), sampleAnswers);
//    }
}
