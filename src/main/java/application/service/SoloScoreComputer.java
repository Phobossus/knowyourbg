package application.service;

import application.model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SoloScoreComputer implements ScoreComputer {

    private static final int WRONG_ANSWER_MODIFIER = -5;
    private static final int CORRECT_ANSWER_MODIFIER = +5;
    private final Map<Player, Score> playerScores = new HashMap<>();

    public SoloScoreComputer(List<Player> playersInGame) {
        playersInGame.forEach(player -> playerScores.put(player, new Score()));
    }

    @Override
    public void compute(Player player, Question question, List<String> selectedAnswers) {
        int points = 0;
        List<String> correctAnswers = question.getCorrectAnswers();
        int correctAnswersSelected = 0;
        int correctAnswersTotal = correctAnswers.size();
        for (String selectedAnswer : selectedAnswers) {
            if (correctAnswers.contains(selectedAnswer)) {
                points += CORRECT_ANSWER_MODIFIER;
                correctAnswersSelected++;
            } else {
                points += WRONG_ANSWER_MODIFIER;
            }
        }
        ScoreData scoreData = new ScoreData(question, points, correctAnswersSelected, correctAnswersTotal);
        Score score = playerScores.get(player);
        score.getQuestionScores().put(question, scoreData);
    }

    @Override
    public Map<Player, Score> getScores() {
        return playerScores;
    }

//    @Override
//    public Map<Player, ScoreData> getScoreForQuestion(Question question) {
//        Map<Player, ScoreData> singleQuestionScore = new HashMap<>();
//        playerScores.keySet().forEach(player -> {
//            ScoreData scoreData = playerScores.get(player).getQuestionScores().get(question);
//            if (scoreData != null) {
//                singleQuestionScore.put(player, scoreData);
//            }
//        });
//        return (singleQuestionScore.size() == playerScores.size()) ? singleQuestionScore : Collections.emptyMap();
//    }

}
