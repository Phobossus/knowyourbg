package application.service;

import application.game.GameMatchManager;
import application.model.Game;
import application.model.Player;
import application.model.SearchState;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class SearchGameService {

    private final GameMatchManager gameMatchManager;

    public SearchGameService(GameMatchManager gameMatchManager) {
        this.gameMatchManager = gameMatchManager;
    }

    public SearchState searchSoloMatch(Player player) {
        if (gameMatchManager.isInRandomSoloQueue(player)) {
            gameMatchManager.removeFromRandomSoloQueue(player);
            return SearchState.CANCELLED;
        }
        gameMatchManager.addToRandomSoloQueue(player);
        return SearchState.PENDING;
    }

    public SearchState getSearchGameState(Player player) {
        if (gameMatchManager.isInRandomSoloQueue(player)) {
            return SearchState.PENDING;
        }
        Optional<Game> dedicatedGame = gameMatchManager.getDedicatedGame(player);
        if (dedicatedGame.isPresent()) {
            return SearchState.FOUND;
        }
        return SearchState.NONEXISTING;
    }

    @PostConstruct
    private void runGameManager() {
        gameMatchManager.startMatchingPlayers();
    }

}
