package application.service;

import application.model.Question;
import application.model.WhoseQuoteIsItQuestion;
import com.google.common.collect.ImmutableMap;

import java.util.*;

// Currently only creates questions from a fixed number of 10 quotes and a fixed number of 17 answers.
public class CharacterQuoteGenerator implements QuestionGenerator {

    private static final String QUESTION_TEXT = "Do kogo należy ten cytat?";
    private static final Map<String, String> quotes = new ImmutableMap.Builder<String, String>()
            .put("Napiłbym się piwa", "Kagain")
            // For debugging reasons
//            .put("Uważaj co do mnie mówisz, albo twe słowa wrócą do ciebie na czubku mojego buta!", "Edwin")
//            .put("Stań przeciw mnie! Stań przeciw nowemu panu zbrodni!", "Sarevok")
//            .put("Wass cass się skońcył, nacelne!", "Sobowtórniak")
//            .put("Zaraz wypruję z ciebie flaki, głupcze!", "Bandyta")
//            .put("Mężczyźni to żałosne istoty!", "Shar-Teel")
//            .put("Woof woof! Wrrrrrr", "Kobold")
            .put("Powinienem był zaciągnąć się do wojska!", "Żołnierz Amn")
            .build();

    private static final List<String> wrongAnswers = Arrays.asList("Napotkana osoba", "Zołnierz Płomiennej Pięsci",
            "Złodziej cienia", "Githyanki", "<CHARNAME>", "Szkielet", "Ogr Berserker");
    private static final List<String> answers = new ArrayList<>();

    static {
        answers.addAll(quotes.values());
        answers.addAll(wrongAnswers);
    }

    @Override
    public List<Question> generate(int count) {
        List<Question> questions = new ArrayList<>();
        quotes.keySet().forEach(quote -> {
            Collections.shuffle(answers);
            Map<String, Boolean> answerMap = new HashMap<>();
            String correctAnswer = quotes.get(quote);
            answerMap.put(correctAnswer, true);
            answers.remove(correctAnswer);
            answers.subList(0, 5).forEach(answer -> answerMap.put(answer, false)); // CONCURRENT MODIFICATION
            answers.add(correctAnswer);
            WhoseQuoteIsItQuestion whoseQuoteIsItQuestion = new WhoseQuoteIsItQuestion(QUESTION_TEXT,
                    quote, answerMap);
            questions.add(whoseQuoteIsItQuestion);
        });
        Collections.shuffle(questions);
        return questions;
    }
}
