package application.service;

import application.model.*;

import java.util.*;
import java.util.stream.Collectors;

// Figure out whether all those synchronizations are necessary
public class SoloGameProgress implements GameProgress {

    private final Map<Question, QuestionStateDetails> questionStates = new HashMap<>();
    private boolean over = false;

    public SoloGameProgress(List<Question> questionList) {
        questionList.forEach(question -> questionStates.put(question, new QuestionStateDetails(QuestionStatus.PENDING)));
        chooseRandomCurrentQuestion();
    }

    @Override
    public synchronized Optional<Question> getCurrentQuestion() {
        return questionStates.keySet().stream()
                .filter(question -> questionStates.get(question).getQuestionStatus().equals(QuestionStatus.CURRENT))
                .findFirst();
    }

    @Override
    public long getRemainingQuestionCount() {
        return questionStates.keySet().stream()
                .filter(question -> !questionStates.get(question).getQuestionStatus().equals(QuestionStatus.COMPLETED))
                .count();
    }

    @Override
    public synchronized void markCurrentQuestionSubmitted(Player player) {
        getCurrentQuestion()
                .map(questionStates::get)
                .map(QuestionStateDetails::getAnswersSubmittedBySet)
                .ifPresent(set -> set.add(player));
    }

    @Override
    public synchronized void markCurrentQuestionCompleted() {
        Optional<Question> currentQuestion = getCurrentQuestion();
        if (currentQuestion.isPresent()) {
            questionStates.put(currentQuestion.get(), new QuestionStateDetails(QuestionStatus.COMPLETED));
            chooseRandomCurrentQuestion();
        }
    }

    @Override
    public synchronized boolean hasAlreadySubmittedAnswer(Player player) {
        return getCurrentQuestion()
                .map(questionStates::get)
                .map(QuestionStateDetails::getAnswersSubmittedBySet)
                .map(set -> set.contains(player))
                .orElse(false);
    }

    @Override
    public synchronized boolean hasEveryPlayerSubmittedAnswer(List<Player> joinedPlayersList) {
        return getCurrentQuestion()
                .map(questionStates::get)
                .map(QuestionStateDetails::getAnswersSubmittedBySet)
                .map(set -> set.containsAll(joinedPlayersList))
                .orElse(false);
    }

    @Override
    public boolean isOver() {
        return over;
    }

    @Override
    public boolean isQuestionCompleted(String questionId) {
        return questionStates.keySet().stream()
                .filter(question -> question.getQuestionId().equals(questionId))
                .map(questionStates::get)
                .anyMatch(state -> state.getQuestionStatus().equals(QuestionStatus.COMPLETED));
    }

    private List<Question> getAllQuestions(QuestionStatus questionStatus) {
        return questionStates.keySet().stream()
                .filter(question ->
                        questionStates.get(question).getQuestionStatus().equals(questionStatus))
                .collect(Collectors.toList());
    }

    private void chooseRandomCurrentQuestion() {
        List<Question> remainingQuestions = getAllQuestions(QuestionStatus.PENDING);
        if (remainingQuestions.size() > 0) {
            Collections.shuffle(remainingQuestions);
            Question currentQuestion = remainingQuestions.get(0);
            questionStates.put(currentQuestion, new QuestionStateDetails(QuestionStatus.CURRENT));
        } else {
            over = true; // misleading place, change in the future
        }
    }

}
