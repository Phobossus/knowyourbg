package application.service;

import application.model.Player;

import java.util.List;

public interface PlayerService {

    Player addPlayer(String username);

    void removePlayer(String username);

    Player getPlayer(String username);

    List<Player> getLoggedPlayers();

}
