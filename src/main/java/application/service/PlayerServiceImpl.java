package application.service;

import application.error.PlayerDoesNotExistException;
import application.model.Player;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {

    private final List<Player> loggedPlayers = new ArrayList<>();
    private final UserService userService;

    public PlayerServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<Player> getLoggedPlayers() {
        return loggedPlayers;
    }

    @Override
    public Player getPlayer(String username) {
        return loggedPlayers.stream()
                .filter(p -> p.getPlayerName().equals(username))
                .findAny()
                .orElse(addPlayer(username));
    }

    @Override
    public Player addPlayer(String username) {
        if (userService.findUser(username).isPresent()) {
            Player player = new Player(username);
            loggedPlayers.add(player);
            return player;
        }
        throw new PlayerDoesNotExistException("Could not create Player of unexisting user: " + username);
    }

    @Override
    public void removePlayer(String username) {
        loggedPlayers.stream()
                .filter(p -> p.getPlayerName().equals(username))
                .findAny()
                .ifPresent(loggedPlayers::remove);
    }

}
