package application.service;

import application.UserServiceResult;
import application.UserValidator;
import application.UsersCache;
import application.mappers.StatisticsMapper;
import application.mappers.UserMapper;
import application.model.db.Statistics;
import application.model.db.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private UserMapper userMapper;
    private StatisticsMapper statisticsMapper;
    private UsersCache usersCache;
    private UserValidator userValidator;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserMapper userMapper, StatisticsMapper statisticsMapper,
                       UsersCache usersCache, UserValidator userValidator, PasswordEncoder passwordEncoder) {
        this.userMapper = userMapper;
        this.statisticsMapper = statisticsMapper;
        this.usersCache = usersCache;
        this.userValidator = userValidator;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public UserServiceResult registerUser(User user) {
        if (userValidator.userExists(user, usersCache.getUsers())) {
            return UserServiceResult.USER_ALREADY_EXISTS;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Statistics statistics = new Statistics();
        statistics.setWins(0);
        statistics.setLoses(0);
        int statResult = statisticsMapper.saveStatistics(statistics);
        user.setStatId(statistics.getId());
        int userResult = userMapper.saveUser(user);
        return (statResult > 0 && userResult > 0) ? UserServiceResult.SUCCESS : UserServiceResult.GENERAL_ERROR;
    }

    public Optional<User> findUser(String username) {
        return Optional.ofNullable(userMapper.selectUserByName(username));
    }

//    // do przemyslenia - moze w cache bedziemy trzymac zalogowanych userow, i think thats better
//    public boolean updateStatistics(String username, boolean won) {
//        User user = userMapper.selectUserByName(username);
//        Statistics statistics = statisticsMapper.selectStatisticsById(user.getStatId());
//        if (won) {
//            statistics.setWins(statistics.getWins() + 1);
//        } else {
//            statistics.setLoses(statistics.getLoses() + 1);
//        }
//        statisticsMapper.saveStatistics()
//        return true;
//    }

    public void deleteUser(User user) {

    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.selectUserByName(username);
        if (user != null) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("DEFAULT_ROLE"));
            return new org.springframework.security.core.userdetails.
                    User(user.getUsername(),
                    user.getPassword(),
                    authorities);
        }
        throw new UsernameNotFoundException("User " + username + " not found. ");
    }

}
