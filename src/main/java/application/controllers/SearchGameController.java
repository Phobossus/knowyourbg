package application.controllers;

import application.model.Player;
import application.model.QueueRequest;
import application.model.SearchStateResponse;
import application.service.AuthenticationService;
import application.service.PlayerService;
import application.service.SearchGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.AccessDeniedException;

@RestController
@RequestMapping("/search")
//TODO: requests should be automatically refused for non-authenticated users - no explicit checking in controllers
public class SearchGameController {

    private final SearchGameService searchGameService;
    private final PlayerService playerService;
    private final AuthenticationService authenticationService;

    @Autowired
    public SearchGameController(SearchGameService searchGameService, AuthenticationService authenticationService,
                                PlayerService playerService) {
        this.searchGameService = searchGameService;
        this.playerService = playerService;
        this.authenticationService = authenticationService;
    }

    @RequestMapping(value = "/solo", method = RequestMethod.POST, consumes = "application/json")
    public SearchStateResponse queueRandomSoloMatch(@RequestBody QueueRequest queueRequest) throws AccessDeniedException {
        System.out.println("Got request: " + queueRequest);
        Authentication authentication = authenticationService.getAuthentication();
        if (authentication.isAuthenticated()) {
            Player player = playerService.getPlayer(authentication.getName());
            return new SearchStateResponse(searchGameService.searchSoloMatch(player));
        }
        throw new AccessDeniedException("You are not logged in.");
    }

    @RequestMapping(value = "/solo/state", method = RequestMethod.GET, produces = "application/json")
    public SearchStateResponse queueGetSearchGameState() throws AccessDeniedException {
        Authentication authentication = authenticationService.getAuthentication();
        if (authentication.isAuthenticated()) {
            Player player = playerService.getPlayer(authentication.getName());
            return new SearchStateResponse(searchGameService.getSearchGameState(player));
        }
        throw new AccessDeniedException("You are not logged in.");
    }

}
