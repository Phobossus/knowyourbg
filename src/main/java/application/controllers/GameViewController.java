package application.controllers;

import application.error.GameDoesNotExistException;
import application.game.GameMatchManager;
import application.model.Game;
import application.model.Player;
import application.service.AuthenticationService;
import application.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.nio.file.AccessDeniedException;
import java.util.Optional;

// TODO: handle anonymous users (does that mean that isAuthenticated returns true for anonymous?)

@Controller
@RequestMapping("/game")
public class GameViewController {

    private final GameMatchManager gameMatchManager;
    private final AuthenticationService authenticationService;
    private final PlayerService playerService;

    @Autowired
    public GameViewController(GameMatchManager gameMatchManager, AuthenticationService authenticationService,
                              PlayerService playerService) {
        this.gameMatchManager = gameMatchManager;
        this.authenticationService = authenticationService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "{gameId}", method = RequestMethod.GET)
    public String viewJoinedGame(@PathVariable("gameId") String gameId) throws AccessDeniedException {
        Authentication authentication = authenticationService.getAuthentication();
        Player player = playerService.getPlayer(authentication.getName());
        Optional<Game> optionalGame = gameMatchManager.getGame(gameId);
        if (optionalGame.isPresent() && isAllowedToView(player, optionalGame.get())) {
            return "game/game";
        }
        throw new GameDoesNotExistException("Game with id " + gameId + " does not exist.");
    }

    private boolean isAllowedToView(Player player, Game game) {
        return game.getExpectedPlayers().contains(player);
    }
}
