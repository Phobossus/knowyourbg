package application.controllers;

import application.error.GameDoesNotExistException;
import application.error.GameNotStartedException;
import application.game.GameMatchManager;
import application.game.PlayGameManager;
import application.model.*;
import application.service.AuthenticationService;
import application.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/game")
public class GameController {

    private final PlayGameManager playGameManager;
    private final GameMatchManager gameMatchManager;
    private final AuthenticationService authenticationService;
    private final PlayerService playerService;

    @Autowired
    public GameController(PlayGameManager playGameManager, GameMatchManager gameMatchManager,
                          AuthenticationService authenticationService, PlayerService playerService) {
        this.playGameManager = playGameManager;
        this.gameMatchManager = gameMatchManager;
        this.authenticationService = authenticationService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "/join", method = RequestMethod.GET, produces = "application/json")
    public JoinedGameResponse joinGame() {
        Player player = playerService.getPlayer(authenticationService.getAuthentication().getName());
        Optional<Game> dedicatedGame = gameMatchManager.getDedicatedGame(player);
        if (dedicatedGame.isPresent()) {
            Game game = dedicatedGame.get();
            game.getJoinedPlayers().add(player);
            playGameManager.initializeGame(game);
            return new JoinedGameResponse(game.getGameId(),
                    game.getExpectedPlayers(),
                    game.getJoinedPlayers(),
                    game.isStarted(),
                    "/game/" + game.getGameId());
        }
        throw new GameDoesNotExistException("Whops, apparently the game no longer exists!");
    }

    @RequestMapping(value = "{gameId}/refresh", method = RequestMethod.GET, produces = "application/json")
    public JoinedGameResponse refreshGame(@PathVariable("gameId") String gameId) {
        Game game = getGame(gameId);
        return new JoinedGameResponse(game.getGameId(),
                game.getExpectedPlayers(),
                game.getJoinedPlayers(),
                game.isStarted(),
                "game/" + game.getGameId());
    }

    @RequestMapping(value = "{gameId}/question", method = RequestMethod.GET, produces = "application/json")
    public NextQuestionResponse nextQuestion(@PathVariable("gameId") String gameId) {
        Game game = getGame(gameId);
        if (game.isStarted()) {
            Optional<Question> currentQuestion = playGameManager.getCurrentQuestion(game);
            if (currentQuestion.isPresent()) {
                Question question = currentQuestion.get();
                if (question instanceof WhoseQuoteIsItQuestion) {
                    return new NextWhoseQuoteIsItResponse(question.getQuestionText(), question.getQuestionObject(),
                            question.getAnswersText(), playGameManager.getRemainingQuestionCount(game));
                }
                throw new IllegalStateException("This question type is not supported");
            } else {
                return getScoresResponse(game);
            }
        }
        throw new GameNotStartedException("Game with id " + gameId + " has not yet started.");
    }

    @RequestMapping(value = "{gameId}/answer", method = RequestMethod.POST, consumes = "application/json")
    public SubmitAnswerResponse submitAnswer(@PathVariable("gameId") String gameId,
                                             @RequestBody List<String> selectedAnswers) {
        Player player = playerService.getPlayer(authenticationService.getAuthentication().getName());
        Game game = getGame(gameId);
        String questionId = playGameManager.submitAnswer(game, player, selectedAnswers);
        return new SubmitAnswerResponse(true, questionId);
    }

    @RequestMapping(value = "{gameId}/{questionId}/completed", method = RequestMethod.GET, produces = "application/json")
    public NextQuestionReadyResponse checkQuestionCompleted(@PathVariable("gameId") String gameId,
                                                            @PathVariable("questionId") String questionId) {
        Game game = getGame(gameId);
        return new NextQuestionReadyResponse(playGameManager.canGetNextQuestion(game, questionId));
    }

    @RequestMapping(value = "{gameId}", method = RequestMethod.DELETE)
    public boolean finalizeGame(@PathVariable("gameId") String gameId) {
        Game game = getGame(gameId);
        return playGameManager.finalizeGame(game);
    }

    private Game getGame(String gameId) {
        Optional<Game> optionalGame = gameMatchManager.getGame(gameId);
        if (optionalGame.isPresent()) {
            return optionalGame.get();
        }
        throw new GameDoesNotExistException("Game with id " + gameId + " does not exist.");
    }

    private NextWhoseQuoteIsItResponse getScoresResponse(Game game) {
        Map<String, Long> totalScores = playGameManager.getScoresTotal(game);
        String winnerText = "ZWYCIĘZCA: " + totalScores.keySet().stream()
                .max(Comparator.comparingLong(totalScores::get)).orElse("REMIS");
        StringBuilder scoresText = new StringBuilder();
        totalScores.forEach((playerName, score) ->
                scoresText.append(playerName).append(": ").append(score).append("\n")
        );
        QuestionObject questionScoresObject = new QuestionObject();
        questionScoresObject.setText(scoresText.toString());
        return new NextWhoseQuoteIsItResponse(winnerText, questionScoresObject,
                null, 0L);
    }
}