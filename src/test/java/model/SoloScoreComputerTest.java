package model;

import application.model.Player;
import application.model.Question;
import application.model.Score;
import application.service.SoloScoreComputer;
import application.service.CharacterQuoteGenerator;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SoloScoreComputerTest {

    @Test
    public void removesPointsForEveryWrongAnswer() {
        List<Player> playersInGame = Arrays.asList(new Player("Kagain94"),
                new Player("McPlayer"));
        List<Question> questions = new CharacterQuoteGenerator().generate(3);

        SoloScoreComputer soloScoreComputer = new SoloScoreComputer(playersInGame);
        Player player = playersInGame.get(0);

        questions.forEach(question -> {
            List<String> threeWrongAnswers = Arrays.asList("wrong", "wrong too", "wrong as well");
            soloScoreComputer.compute(player, question, threeWrongAnswers);
        });

        Map<Player, Score> scores = soloScoreComputer.getScores();
    }
}
