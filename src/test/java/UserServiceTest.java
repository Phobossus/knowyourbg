import application.model.db.Statistics;
import application.model.db.User;
import application.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void savesUser() {
        Statistics statistics = new Statistics();
        statistics.setWins(5);
        statistics.setLoses(2);
        User user = new User();
        user.setUsername("mateo53");
        user.setPassword("digitcalcrap");
        user.setEmail("michal@o2.pl");
        user.setStatistics(statistics);
        userService.registerUser(user);
    }


}
