import application.UsersCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class UsersCacheTest {

    @Autowired
    private UsersCache usersCache;

    @Test
    public void shouldLoadAllUsersFromDb() {
        assertNotNull(usersCache.getUsers());
    }
}
